//
//  ToDoTask+CoreDataProperties.swift
//  
//
//  Created by Anton on 01.05.2020.
//
//

import Foundation
import CoreData


extension ToDoTaskCoreData {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<ToDoTaskCoreData> {
        return NSFetchRequest<ToDoTaskCoreData>(entityName: "ToDoTaskCoreData")
    }

    @NSManaged public var task: String?

}
