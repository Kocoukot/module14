

import UIKit
import RealmSwift



class ToDoViewController: UIViewController {
    private let realm = try! Realm()
    
    @IBOutlet weak var toDoTaskTextField: UITextField!
    @IBOutlet weak var toDoTable: UITableView!
    var toDoList = List<String>()

    

    
    override func viewDidLoad() {
        super.viewDidLoad()
        if realm.objects(ToDoTaskList.self).first != nil{
        let tasks = realm.objects(ToDoTaskList.self).last!

            for i in 0...tasks.toDoList.count-1{
            toDoList.append(tasks.toDoList[i].toDoTask)
        }
        }

    }
    

    @IBAction func addTaskButton(_ sender: Any) {
        if !toDoTaskTextField.text!.isEmpty{
        let newToDoTask = String(toDoTaskTextField.text!)
        toDoTaskTextField.text = ""
        toDoList.append(newToDoTask)
        let indexPath = IndexPath(row: toDoList.count - 1, section: 0)
        toDoTable.beginUpdates()
            toDoTable.insertRows(at: [indexPath], with: .automatic)
        toDoTable.endUpdates()
        }
    }
    
    @IBAction func delTaskButton(_ sender: Any) {
        
        if let selectedRows = toDoTable.indexPathsForSelectedRows {
            var items = [String]()
            for indexPath in selectedRows  {
                items.append(toDoList[indexPath.row])
            }
            for item in items {
                if let index = toDoList.index(of: item) {
                    toDoList.remove(at: index)
                }
            }
            toDoTable.beginUpdates()
            toDoTable.deleteRows(at: selectedRows, with: .automatic)
            toDoTable.endUpdates()
        }
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
              listSave()
    }
    
        func listSave(){
            let toDoListRealm = ToDoTaskList()
            let tempList = List<ToDoTask>()
            for task in toDoList{
                let toDoTaskRealm = ToDoTask()
                toDoTaskRealm.toDoTask = task
                tempList.append(toDoTaskRealm)
            }
            toDoListRealm.toDoList = tempList
                try! realm.write{
                    realm.add(toDoListRealm)
            }
        }
    

    
}


extension ToDoViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return toDoList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "toDoTask") as! TableViewCell
        cell.ToDoTaskLable.text = toDoList[indexPath.row]
        return cell
    }
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == UITableViewCell.EditingStyle.delete {
            toDoList.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: UITableView.RowAnimation.automatic)
        }
    }
    
   }

