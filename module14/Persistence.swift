
import Foundation

class Persistence{
    static let shared = Persistence()
    
    private let userNameKey = "Persistence.userNameKey"
    
    var userName: String?{
        set { UserDefaults.standard.set(newValue, forKey: userNameKey) }
        get { return UserDefaults.standard.string(forKey: userNameKey) }
    }
    
    private let userSecondNameKey = "Persistence.userSecondNameKey"
    
    var userSecondName: String?{
        set { UserDefaults.standard.set(newValue, forKey: userSecondNameKey) }
        get { return UserDefaults.standard.string(forKey: userSecondNameKey) }
    }
    
    
}
