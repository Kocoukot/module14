//
//  ViewController.swift
//  module14
//
//  Created by Anton on 25.04.2020.
//  Copyright © 2020 Anton. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var nameLabel: UITextField!
    @IBOutlet weak var SecondNameLabel: UITextField!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        nameLabel.text = Persistence.shared.userName
        SecondNameLabel.text = Persistence.shared.userSecondName

    }

   override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
    Persistence.shared.userName = nameLabel.text
    Persistence.shared.userSecondName = SecondNameLabel.text
    }

}

