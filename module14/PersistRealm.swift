


import Foundation
import RealmSwift


class ToDoTask: Object{
    @objc dynamic var toDoTask = ""
    @objc dynamic var owner: ToDoTaskList?
}

class ToDoTaskList: Object{
     var toDoList = List<ToDoTask>()

}
