//
//  CoreDataViewController.swift
//  module14
//
//  Created by Anton on 01.05.2020.
//  Copyright © 2020 Anton. All rights reserved.
//

import UIKit
import CoreData

class CoreDataViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var textField: UITextField!
    var toDoListCore: [ToDoTaskCoreData] = []
    
    
    override func viewWillAppear(_ animated: Bool) {
      super.viewWillAppear(true)
        let appDelegate = (UIApplication.shared.delegate as! AppDelegate)
        let context = appDelegate.persistentContainer.viewContext
        
        let fetchRequest: NSFetchRequest<ToDoTaskCoreData> = ToDoTaskCoreData.fetchRequest()
        do {
            toDoListCore = try context.fetch(fetchRequest)
        } catch let error as NSError{
            print ("error")
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
 
    
    @IBAction func addTaskNButton(_ sender: Any) {
       if !textField.text!.isEmpty{

        let newToDoTask = String(textField.text!)
        saveTask(name: newToDoTask)
        textField.text = ""
        }
        
}
    
    func saveTask(name: String) {
        let appDelegate = (UIApplication.shared.delegate as! AppDelegate)
        let context = appDelegate.persistentContainer.viewContext
       
        guard let entity = NSEntityDescription.entity(forEntityName: "ToDoTaskCoreData", in: context) else {return}
        let task = ToDoTaskCoreData(entity: entity, insertInto: context)
        task.taskCore = name
        do {
            try context.save()
            self.toDoListCore.append(task)
            tableView.reloadData()
        } catch let error as NSError{
            print ("error")
        }
        
        
    }
    
    @IBAction func delTaskButton(_ sender: Any) {
        let appDelegate = (UIApplication.shared.delegate as! AppDelegate)
        let context = appDelegate.persistentContainer.viewContext
        let fetchRequest: NSFetchRequest<ToDoTaskCoreData> = ToDoTaskCoreData.fetchRequest()
        
        if let selectedRows = tableView.indexPathsForSelectedRows {
            var items = [ToDoTaskCoreData]()
            for indexPath in selectedRows  {
                items.append(toDoListCore[indexPath.row])
            }
            
            if let tasks = try? context.fetch(fetchRequest){
                for item in items {
                    if let index = toDoListCore.firstIndex(of: item) {
                        context.delete(item)
                        toDoListCore.remove(at: index)
                    }
            }
            tableView.beginUpdates()
            tableView.deleteRows(at: selectedRows, with: .automatic)
            tableView.endUpdates()
        }
        do {
            try context.save()
        } catch let error as NSError{
            print ("error")
        }
    }
}
}


extension CoreDataViewController: UITableViewDataSource {
 func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
     return toDoListCore.count
 }
 
 func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
     let cell = tableView.dequeueReusableCell(withIdentifier: "toDoTaskCore") as! TableViewCell
    let task = toDoListCore[indexPath.row]
    cell.ToDoTaskLable.text = task.taskCore

     return cell
 }
    
 func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
    
    if editingStyle == UITableViewCell.EditingStyle.delete {
        let appDelegate = (UIApplication.shared.delegate as! AppDelegate)
        let context = appDelegate.persistentContainer.viewContext
        let fetchRequest: NSFetchRequest<ToDoTaskCoreData> = ToDoTaskCoreData.fetchRequest()
        let taskToDel = toDoListCore[indexPath.row]
            context.delete(taskToDel)
            do {
                       try context.save()
                   } catch let error as NSError{
                       print ("error")
                   }
         toDoListCore.remove(at: indexPath.row)
         tableView.deleteRows(at: [indexPath], with: UITableView.RowAnimation.automatic)
     }
 
    }
}
